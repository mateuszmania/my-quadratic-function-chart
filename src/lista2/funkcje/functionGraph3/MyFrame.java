package lista2.funkcje.functionGraph3;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MyFrame {

	public static void open() {
		final JFrame okno = new JFrame("Wykres funkcji");
		okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		okno.setSize(800, 600);
		okno.setVisible(true);
		okno.setLayout(new FlowLayout());

		JPanel panelKontrolny = new JPanel();
		panelKontrolny.setLayout(new GridLayout());

		JPanel panel3 = new JPanel();
		panel3.setLayout(new FlowLayout());

		JTextField ta = new JTextField("1");
		JTextField tb = new JTextField("0");
		JTextField tc = new JTextField("0");
		JButton jb = new JButton("Rysuj");

		JLabel opis0 = new JLabel(" ", JLabel.RIGHT);
		JLabel opis1 = new JLabel(" y = ", JLabel.RIGHT);
		JLabel opis1a = new JLabel("*   (x^2)   +   ", JLabel.CENTER);
		JLabel opis1b = new JLabel(" * x +", JLabel.CENTER);

		panelKontrolny.add(opis0);
		panelKontrolny.add(opis1);
		panelKontrolny.add(ta);
		panelKontrolny.add(opis1a);
		panelKontrolny.add(tb);
		panelKontrolny.add(opis1b);
		panelKontrolny.add(tc);
		
		panel3.add(jb);

		MyPanel myPanel = new MyPanel(ta, tb, tc);
		okno.add(myPanel);
		okno.add(panelKontrolny);
		okno.add(panel3);
		jb.addActionListener(myPanel);
		
		okno.setSize(new Dimension(800, 500));
		okno.setVisible(true);
	}

}
