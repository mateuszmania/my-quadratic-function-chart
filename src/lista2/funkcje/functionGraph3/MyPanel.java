package lista2.funkcje.functionGraph3;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JTextField;

class MyPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = 7964852187041889953L;
	JTextField ta;
	JTextField tb;
	JTextField tc;
	final static Color kolorRamki = Color.black;
	final static Color kolorTla = Color.LIGHT_GRAY;
	final static Color kolorLiter = Color.black;

	public MyPanel(JTextField _a, JTextField _b, JTextField _c) {
		ta = _a;
		tb = _b;
		tc = _c;
		setPreferredSize(new Dimension(700, 400));
	}

	public Double[] pierwiastki(double a, double b, double c) {
		Double[] wynik = new Double[2];
		if (a != 0) {
			double delta = ((b * b) - 4 * a * c);
			if (delta > 0) {
				wynik[0] = (-b - Math.sqrt(delta)) / (2 * a);
				wynik[1] = (-b + Math.sqrt(delta)) / (2 * a);
			} else if (delta == 0) {
				wynik[0] = (-b / (2 * a));
				wynik[1] = null;
			} else {
				wynik[0] = null;
				wynik[1] = null;
			}
		}
		return wynik;
	}

	protected void paintComponent(Graphics g) {
		super.paintComponent(g);

		g.setColor(kolorTla);
		g.fillRect(0, 0, 699, 399);
		g.setColor(kolorRamki);
		g.drawRect(0, 0, 699, 399);

		g.drawLine(20, 200, 680, 200);
		g.drawLine(350, 20, 350, 380);

		g.drawLine(680, 200, 660, 190);
		g.drawLine(680, 200, 660, 210);
		g.drawLine(350, 20, 340, 40);
		g.drawLine(350, 20, 360, 40);

		g.drawString("X", 670, 230);
		g.drawString("Y", 370, 30);

		Polygon p = new Polygon();

		double a = Double.parseDouble(ta.getText());
		double b = Double.parseDouble(tb.getText());
		double c = Double.parseDouble(tc.getText());

		for (int x = -330; x <= 330; x++) {
			p.addPoint(20 * x + 350, (200 - ((int) (20 * (a * (x * x) + b * (x) + c)))));
		}

		g.setColor(Color.red);
		g.drawPolyline(p.xpoints, p.ypoints, p.npoints);

		Font f = new Font("TimesRoman", Font.PLAIN, 9);
		g.setFont(f);

		for (int i = 50; i < 650; i += 20) {
			g.setColor(Color.black);
			g.drawLine(i, 203, i, 197);
			g.drawString("" + ((i - 350) / 20) + "", i, 215);
		}

		for (int i = 60; i < 360; i += 20) {
			g.setColor(Color.black);
			g.drawLine(347, i, 353, i);
			if (i != 200) {
				g.drawString("" + ((200 - i) / 20) + "", 360, i);
			}
		}

		Font f2 = new Font("TimesRoman", Font.BOLD, 11);
		g.setFont(f2);
		g.setColor(Color.white);

		if (pierwiastki(a, b, c)[0] != null && pierwiastki(a, b, c)[1] != null) {
			g.fillOval(((int) (pierwiastki(a, b, c)[0] * 20 + 350 - 3)), 200 - 3, 6, 6);
			g.drawOval(((int) (pierwiastki(a, b, c)[0] * 20 + 350 - 3)), 200 - 3, 6, 6);
			g.drawString("" + Math.round(pierwiastki(a, b, c)[0] * 100.0) / 100.0 + "",
					(int) (pierwiastki(a, b, c)[0] * 20 + 350 + 5), 200 - 5);
			g.fillOval(((int) (pierwiastki(a, b, c)[1] * 20 + 350 - 3)), 200 - 3, 6, 6);
			g.drawOval(((int) (pierwiastki(a, b, c)[1] * 20 + 350 - 3)), 200 - 3, 6, 6);
			g.drawString("" + Math.round(pierwiastki(a, b, c)[1] * 100.0) / 100.0 + "",
					(int) (pierwiastki(a, b, c)[1] * 20 + 350 + 5), 200 - 5);
		} else if (pierwiastki(a, b, c)[0] != null && pierwiastki(a, b, c)[1] == null) {
			g.fillOval(((int) (pierwiastki(a, b, c)[0] * 20 + 350 - 3)), 200 - 3, 6, 6);
			g.drawOval(((int) (pierwiastki(a, b, c)[0] * 20 + 350 - 3)), 200 - 3, 6, 6);
			g.drawString("" + Math.round(pierwiastki(a, b, c)[0] * 100.0) / 100.0 + "",
					(int) (pierwiastki(a, b, c)[0] * 20 + 350 + 5), 200 - 5);
		} else {
			
		}

	}

	public void actionPerformed(ActionEvent e) {
		getParent().repaint();
	}
}
